module Str2Hask (hsparse, xparse) where

import Text.ParserCombinators.Parsec
import qualified Text.ParserCombinators.Parsec.Token as PT
import Text.ParserCombinators.Parsec.Language (emptyDef)

import Hask

hsparse :: String -> Either ParseError HS
hsparse s = parse (parse_hs <* eof) "" s
xparse :: String -> Either ParseError [String]
xparse s = parse (parse_data_head <* eof) "" s

--tt s = parse (parse_kind <* eof) "" s

-- $setup
-- >>> :set -XFlexibleContexts
-- >>> let pt p = parse (p <* eof) ""

-- | function generating a token parser based on a
-- lexical parsers combined with a language record definition
lexer :: PT.TokenParser st
lexer  = PT.makeTokenParser emptyDef

-- | token parser for parenthesis
parens :: CharParser st a -> CharParser st a
parens = PT.parens lexer

squares :: CharParser st a -> CharParser st a
squares = PT.squares lexer

-- | token parser for identifiers
-- >>> pt identifier "asdf"
-- Right "asdf"
identifier :: CharParser st String
identifier = PT.identifier lexer

-- | token parser for keywords
reservedOp :: String -> CharParser st ()
reservedOp = PT.reservedOp lexer

-- | haskell declaration parser
--
-- >>> pt parse_hs "data X :: *"
-- Right (HSKind (Kind "X" 0))
--
-- >>> pt parse_hs "data X a :: * -> *"
-- Right (HSKind (Kind "X" 1))
parse_hs :: CharParser () HS
parse_hs = try (do { d <- parse_decl; return $ HSDecl d }) <|>
           try (do { d <- parse_data; return $ HSData d }) <|>
           try (do { k <- parse_kind; return $ HSKind k }) <|>
                do { t <- parse_typealias; return $ HSType t }

----------------- Parse Type ----------------------------------
parse_type :: CharParser () Type
parse_type = type_term `chainr1` fun_action

type_term :: CharParser () Type
type_term = do { a <- many1 factor; return $ if length a == 1 then head a else Arged a }

head_term :: CharParser () Type
head_term = do { i <- identifier; return $ Plain i }

list_term :: CharParser () Type
list_term = do { t <- squares parse_type; return $ Arged [Plain "[]", t] }

tuple_term :: CharParser () Type
tuple_term = do { reservedOp "("; t <- sepBy type_term (reservedOp ","); reservedOp ")"; return $ if t == [] then Plain "()" else if length t == 1 then head t else Tuple t }

{-unit_term :: CharParser () Type
unit_term = do { string "()"; many1 (string " "); return $ Plain "()" }-}

factor :: CharParser () Type
factor = {-try unit_term <|> -}try tuple_term <|> list_term <|> head_term <|> parens parse_type

fun_action :: CharParser () (Type -> Type -> Type)
fun_action = do { reservedOp "->"; return $ Fun }

----------------- Parse Typeclass ----------------------------------
parse_typeclasses :: CharParser () [Typeclass]
parse_typeclasses = sepBy typeclass_term (reservedOp ",")

typeclass_term :: CharParser () Typeclass
typeclass_term = do { n <- identifier; a <- many1 identifier; return $ Typeclass n a }

----------------- Parse Declaration ----------------------------------
parse_decl :: CharParser () Decl
parse_decl = try (do { n <- identifier; reservedOp "::"; tc <- parens parse_typeclasses; reservedOp "=>"; t <- parse_type; return $ Decl n tc t }) <|>
             try (do { n <- identifier; reservedOp "::"; tc <- typeclass_term; reservedOp "=>"; t <- parse_type; return $ Decl n [tc] t }) <|>
             do { n <- identifier; reservedOp "::"; t <- parse_type; return $ Decl n [] t }

----------------- Parse Data Declaration ----------------------------------
-- |
-- >>> pt parse_data "data X = C"
-- Right (Data "X" [] [Ctor "C" []])
--
-- >>> pt parse_data "data X = C1 | C2"
-- Right (Data "X" [] [Ctor "C1" [],Ctor "C2" []])
--
-- >>> pt parse_data "data X = C1 a b | C2 x"
-- Right (Data "X" [] [Ctor "C1" [(Nothing,(Pa)),(Nothing,(Pb))],Ctor "C2" [(Nothing,(Px))]])
--
-- >>> pt parse_data "data X = X { a :: B }"
-- Right (Data "X" [] [Ctor "X" [(Just "a",(PB))]])
parse_data :: CharParser () Data
parse_data = do
  h <- parse_data_head
  reservedOp "="
  c <- sepBy ctor_term (reservedOp "|")
  return $ Data (head h) (tail h) c

parse_data_head :: CharParser () [String]
parse_data_head = try (do { reservedOp "data"; i <- many1 identifier; reservedOp "::"; _ <- sepBy (reservedOp "*" <|> reservedOp "Type") (reservedOp "->"); return i }) <|>
                  do { reservedOp "data"; many1 identifier }

record_term :: CharParser () Ctor
record_term = do { i <- identifier; reservedOp "{"; f <- sepBy field_term (reservedOp ";"); reservedOp "}"; return $ Ctor i f }

field_term :: CharParser () (Maybe String, Type)
field_term = do { extractor <- identifier; reservedOp "::"; t <- parse_type; return (Just extractor, t) }

ctor_ap_term :: CharParser () Ctor
ctor_ap_term = do { l <- many1 parse_type; case l of { [Plain i] -> return $ Ctor i []; [Arged ((Plain i):ll)] -> return $ Ctor i (map (\t -> (Nothing, t)) ll); _ -> fail "wrong ctor" } }

ctor_term :: CharParser () Ctor
ctor_term = try record_term <|>
            try ctor_ap_term

----------------- Parse Type Alias ----------------------------------
-- |
-- >>> pt parse_typealias "type X = Y"
-- Right (Type2 "X" [] (PY))
--
-- >>> pt parse_typealias "type X a = Y a"
-- Right (Type2 "X" ["a"] (A[(PY), (Pa)]))
parse_typealias :: CharParser () Type2
parse_typealias = do { reservedOp "type"; i <- many1 identifier; reservedOp "="; t <- parse_type; return $ Type2 (head i) (tail i) t }

----------------- Parse Kinds ----------------------------------
-- |
-- >>> pt parse_kind "data X :: *"
-- Right (Kind "X" 0)
--
-- >>> pt parse_kind "data X :: Type"
-- Right (Kind "X" 0)
--
-- >>> pt parse_kind "data X a :: * -> *"
-- Right (Kind "X" 1)
parse_kind :: CharParser () Kind
parse_kind = try (do { reservedOp "data"; i <- many1 identifier; reservedOp "::"; _ <- sepBy (reservedOp "Type" <|> reservedOp "*") (reservedOp "->"); return $ Kind (head i) (toInteger $ length (tail i)) }) <|>
                  do { reservedOp "data"; i <- many1 identifier; return $ Kind (head i) (toInteger $ length (tail i)) }
