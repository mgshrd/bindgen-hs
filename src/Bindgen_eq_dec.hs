{-# LANGUAGE CPP #-}

module Bindgen_eq_dec (NonDec, addDec, getDec, genLtac) where

import qualified Coq as C

import qualified Data.List as L
import qualified Data.Set as S

import qualified Data.Semigroup as SG
import qualified Data.Traversable as T

type NonDec = String -> Maybe (S.Set String)

newtype TracedResult a = TracedResult { trVal :: Either String a } deriving (Show)

instance SG.Semigroup a => SG.Semigroup (TracedResult a) where
  (<>) (TracedResult (Right a)) (TracedResult (Right b)) = TracedResult $ Right $ a SG.<> b
  (<>) (TracedResult (Left s)) (TracedResult (Right _)) = TracedResult $ Left s
  (<>) (TracedResult (Right _)) (TracedResult (Left s)) = TracedResult $ Left s
  (<>) (TracedResult (Left s1)) (TracedResult (Left s2)) = TracedResult $ Left (s1 ++ "/" ++ s2)

instance Functor TracedResult where
  fmap f m = TracedResult $ fmap f (trVal m)

instance Applicative TracedResult where
  pure a = TracedResult $ Right a
  (<*>) f v = TracedResult $ (trVal f) <*> (trVal v)

instance Monad TracedResult where
  return = pure
  (>>=) v f = TracedResult $ (trVal v) >>= (trVal . f)

flattenM2b :: Maybe (TracedResult a) -> Either String a
flattenM2b (Just x) = trVal x
flattenM2b Nothing = Left "???"

getDec' :: [String] -> NonDec -> C.Expr -> Maybe (TracedResult (S.Set C.DecEqFun))
getDec' avoid nonDec =
  --let s2d = \s -> if S.member s isDec then TracedResult $ Just $ S.fromList [C.DecEqFun $ if s == "list" then "List.list_eq_dec" else "eq_" ++ s ++ "_dec"] else TracedResult Nothing in
  let m2e m b = case m of { Just a -> Right a; Nothing -> Left b } in
  let s2d s = TracedResult $ if L.elem s $ "_":avoid
                       then Right $ S.fromList []
                       else m2e (fmap (S.fromList . fmap C.DecEqFun . S.toList) $ nonDec s) s in
  C.memap (\e -> case e of
                        C.Term s -> s2d s
                        C.TypedTerm s _ -> s2d s
                        C.Arged l -> fmap mconcat $ T.sequence $ fmap (TracedResult . flattenM2b . getDec' avoid nonDec) l
                        C.Tuple l -> fmap mconcat $ T.sequence $ fmap (TracedResult . flattenM2b . getDec' avoid nonDec) l
                        e' -> TracedResult $ Left $ show e')

getDec :: [String] -> NonDec -> C.Expr -> Either String [C.DecEqFun]
getDec avoid nonDec e = 
  let res = getDec' avoid nonDec e in
  fmap S.toList $ flattenM2b res

getDecS :: [String] -> NonDec -> C.Statement -> Either String [C.DecEqFun]
getDecS avoid nonDec s = 
  let ss = C.smemap (getDec' avoid nonDec) s in
  case ss of
    Just v -> fmap S.toList $ flattenM2b v
    Nothing -> Right [] -- no expressions in statement

addDec :: NonDec -> [C.Statement] -> [C.Statement]
addDec nonDec = concat . map (addDecS nonDec)

addDecS :: NonDec -> C.Statement -> [C.Statement]
addDecS _ sx@(C.Axiom s (C.Term "Set")) =
  [sx, C.Axiom ("eq_" ++ s ++ "_dec") (C.Forall [("a b", Just $ C.Term s)] (C.Term "{a=b}+{a<>b}"))]
addDecS _ sx@(C.Axiom s (C.Fun (C.Term "Set") (C.Term "Set"))) = -- all :: * -> * types assumed to be Eq if their arg is Eq
  [sx, C.Axiom ("eq_" ++ s ++ "_dec")
      (C.Forall [
         ("A", Just $ C.Term "Set"),
         ("eq_A_dec", Just $ C.Term "forall a b : A, {a=b}+{a<>b}"),
         ("a b", Just $ C.Arged [C.Term s, C.Term "A"])
        ]
        (C.Term "{a=b}+{a<>b}"))]
addDecS nonDec sx@(C.Definition n e) =
  [sx] ++ case getDec [n] nonDec e of {
                     Left n' -> [C.Comment $ "undecdef: " ++ n' ++ "/" ++ show sx];
                     Right l ->
                       let ltac = genLtac "" "" l in
                       let body = ltac ++ " : forall a b : " ++ n ++ ", {a=b}+{a<>b}" in
                       [C.Definition ("eq_" ++ n ++ "_dec") (C.Term body)]
                   }
addDecS nonDec sx@(C.Inductive n a _) =
   [sx{-, C.Comment $ __FILE__ ++ ":" ++ show __LINE__ ++ ":" ++ show sx-}] ++ case getDecS (n:map fst a) nonDec sx of {
             Left s -> [C.Comment $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ ":undecind: " ++ s ++ "/" ++ show sx];
             Right l ->
               let args = L.intercalate " " $ map fst a in
               let type_ = "forall a b : " ++ n ++ (if args == "" then "" else " ") ++ args ++ ", {a=b}+{a<>b}" in
               let ltac = genLtac "" "" l in
               let head_ = if a == [] then "" else "fun " ++ args ++ " => " in
               let body = head_ ++ ltac ++ " : " ++ type_ in
               [C.Definition ("eq_" ++ n ++ "_dec") (C.Term body)]}
addDecS _ sx@(C.ExtractConstant _ _) = [sx]
addDecS _ sx@(C.ExtractInductive _ _ _) = [sx]
addDecS _ sdx = [C.Comment $ "eq_XxxxxX_dec: " ++ show sdx, sdx]

genLtac :: String -> String -> [C.DecEqFun] -> String
genLtac intro outro l =
  let eqs = (foldr (\ (C.DecEqFun fn) a -> a ++ ("\nassert (H" ++ map (\c -> if c == '.' || c == '@' then '_' else c) fn ++ " := " ++ fn ++ "); ")) "" l) in
  let tacs = map (\t -> "solve [ " ++ t ++ " ]") ["decide equality", "firstorder", "repeat decide equality"] in
  let preproc = "repeat match goal with |- forall _ _ : ?A * ?B, _ => intros; apply @Beque.Util.dec_utils.eq_prod_dec end;\n" ++
                "try match goal with X : forall _, _ -> forall _ _ : ?x _, _ |- forall _ _ : ?x _, _ => apply X end;" in
  let solve = "first [ " ++ L.intercalate " | " tacs ++ " ]" in
  let ltac = "ltac:(\n" ++ intro ++ "\n" ++ eqs ++ "\n" ++ outro ++ "\n" ++ preproc ++ "\n" ++ solve ++ ")" in
  ltac
