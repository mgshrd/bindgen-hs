{-# LANGUAGE CPP #-}

module Coq (
  Expr(Term, TypedTerm, Arged, Tuple, Fun, Forall, Funall, Apply),
  Ctor(Ctor),
  Statement(Comment, Axiom, Definition, Inductive, ExtractConstant, ExtractInductive),
  DecEqFun(DecEqFun),
  semap, sstemap, stemap, memap, smemap, emap, rename, replace,
  inductiveType, inductiveType',
  arity, arity',
  getBottom, replaceBottom, argNames, args,
) where

import Data.Maybe
import Control.Monad.State

data Expr
  = Term String
  | TypedTerm String Expr
  | Arged [Expr] -- redundant === Apply (Apply ... [0]) [n])
  | Tuple [Expr] -- redundant === Apply ((Apply "(fun a b => a * b)%type") [0]) ... [n])
  | Fun Expr Expr -- redundant === Apply ((Apply "(fun a b => a -> b)") [0]) [1])
  | Forall [(String, Maybe Expr)] Expr
  | Funall [(String, Maybe Expr)] Expr
  | Apply Expr Expr
    deriving (Eq, Ord, Show)
data Ctor = Ctor String [(Maybe String, Maybe Expr)] deriving (Eq, Ord, Show)
data Statement
  = Comment String
  | Axiom String Expr
  | Definition String Expr
  | Inductive String [(String, Maybe Expr)] [Ctor]
  | ExtractConstant String String
  | ExtractInductive String [String] (Maybe String)
    deriving (Eq, Ord, Show)
newtype DecEqFun = DecEqFun String deriving (Eq, Ord, Show)

semap :: (Expr -> Expr) -> Statement -> Statement
semap f (Axiom s e) = Axiom s (f e)
semap f (Definition s e) = Definition s (f e)
semap f (Inductive s pl cl) =
  Inductive s
    (map (\ p -> (fst p, case snd p of { Nothing -> Nothing; Just e -> Just $ f e })) pl)
    (map (\ (Ctor cs el) -> Ctor cs $ fmap (fmap (fmap f)) el) cl)
semap _ s = s

sstemap :: (Expr -> State a Expr) -> Statement -> State a Statement
sstemap f (Axiom s e) = fmap (Axiom s) (stemap f e)
sstemap f (Definition s e) = fmap (Definition s) (stemap f e)
sstemap f (Inductive is pl cl) =
  do
    pl' <- sequence $ map (\ p -> fmap (\e -> (fst p, e)) (case snd p of { Nothing -> return Nothing; Just e -> fmap Just $ stemap f e })) pl
    cl' <- sequence $ map (csstemap f) cl
    return $ Inductive is pl' cl'
sstemap _ s = return s
csstemap :: (Expr -> State a Expr) -> Ctor -> State a Ctor
csstemap f (Ctor s el) =
  let mapElem = traverse (stemap f) in
  let mapSeq = traverse (\ p -> fmap (\me -> (fst p, me)) $ mapElem (snd p)) in
  fmap (Ctor s) $ mapSeq el

stemap :: (Expr -> State a Expr) -> Expr -> State a Expr
stemap f e@(Term _) = f e
stemap f (TypedTerm s e) = do { e' <- stemap f e; f $ TypedTerm s e' }
stemap f (Arged l) = do { l' <- mapM (stemap f) l; f $ Arged l' }
stemap f (Tuple l) = do { l' <- mapM (stemap f) l; f $ Tuple l' }
stemap f (Fun d cd) = do { d' <- stemap f d; cd' <- stemap f cd; f $ Fun d' cd' }
stemap f (Forall l e) = do { l' <- mapM (sttup $ stemap f) l; e' <- stemap f e; f $ Forall l' e' }
stemap f (Funall l e) = do { l' <- mapM (sttup $ stemap f) l; e' <- stemap f e; f $ Funall l' e' }
stemap f (Apply e1 e2) = do { e1' <- stemap f e1; e2' <- stemap f e2; f $ Apply e1' e2' }

sttup :: (Expr -> State a Expr) -> (String, Maybe Expr) -> State a (String, Maybe Expr)
sttup f (s, Just e) = do { e' <- f e; return (s, Just e') }
sttup _ (s, Nothing) = return (s, Nothing)

emap :: (Expr -> Expr) -> (Expr -> Expr)
emap f = flip evalState () . stemap (return . f)

memap :: Semigroup m => (Expr -> m) -> (Expr -> Maybe m)
memap f = flip execState Nothing . stemap (\e -> do {
        a <- get;
        let rr = if isJust a then fmap (<> f e) a else Just $ f e in
        put rr;
        return e })

smemap :: Semigroup m => (Expr -> m) -> (Statement -> Maybe m)
smemap f = flip execState Nothing . sstemap (\e -> do {
        a <- get;
        let rr = if isJust a then fmap (<> f e) a else Just $ f e in
        put rr;
        return e })

rename :: String -> String -> (Expr -> Expr)
rename from to = emap (\e -> case e of { Term s | s == from -> Term to; x -> x })

replace ::  Expr -> Expr -> (Expr -> Expr)
replace old new = emap (\e -> if e == old then new else e)

arity :: Expr -> Int
arity = length . arity'

arity' :: Expr -> [String]
arity' (Term _) = []
arity' (TypedTerm _ _) = []
arity' (Arged _) = []
arity' (Tuple _) = []
arity' (Fun _ e) = "_":arity' e
arity' (Forall a e) = map fst a ++ arity' e
arity' (Funall a e) = map fst a ++ arity' e
arity' (Apply e _) = init $ arity' e

argNames :: Int -> [(Maybe String, a)] -> [String]
argNames _ [] = []
argNames n ((Just an, _):t) = an:argNames n t
argNames n ((Nothing, _):t) = ("a" ++ show n):argNames (n + 1) t

getBottom :: Expr -> Expr
getBottom (Fun _ cd) = getBottom cd
getBottom (Forall _ e) = getBottom e
getBottom (Funall _ e) = getBottom e
getBottom e = e

args :: Expr -> [Either (String, Expr) (Either String Expr)]
args (Fun d cd) = Right (Right d):args cd
args (Funall a fe) = map (\ (n, me) -> case me of { Just e -> Left (n, e); Nothing -> Right (Left n) }) a ++ args fe
args (Forall a fe) = map (\ (n, me) -> case me of { Just e -> Left (n, e); Nothing -> Right (Left n) }) a ++ args fe
args e = [Right (Right e)]

replaceBottom :: Expr -> Expr -> Expr
replaceBottom cd' (Fun d cd@(Fun _ _)) = Fun d (replaceBottom cd' cd)
replaceBottom cd' (Fun d _) = Fun d cd'
replaceBottom _ _ = error "cannot replace bottom of non Fun"

inductiveType' :: String -> [(String, Maybe Expr)] -> Expr
inductiveType' n a = if a == [] then Term n else (Arged $ map Term (n:map fst a))

inductiveType :: Statement -> Maybe Expr
inductiveType (Inductive n a _) = Just $ inductiveType' n a
inductiveType _ = Nothing
