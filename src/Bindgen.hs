{-# LANGUAGE CPP #-}

module Bindgen (bindgen) where

import qualified Bindgen_eq_dec as ED

import qualified Str2Hask as S2H
import qualified Hask as H
import qualified Hask2Coq as H2C
import qualified Coq as C
import qualified Coq2Coq as C2C
import qualified Coq2Str as C2S

import qualified Data.Either as E
import qualified Data.List as L
import qualified Data.Maybe as M
import qualified Data.Set as S
import qualified Data.Traversable as T

import qualified Control.Monad.State as St

import qualified Text.Parsec.Error
import qualified Data.Bifunctor as BF

bindgen :: ED.NonDec -> String -> [String] -> Either String String
bindgen nonDec modul lins =
  let hs = map S2H.hsparse lins
           :: [Either Text.Parsec.Error.ParseError H.HS] in
  let hs' = map (\(l, eh) -> BF.bimap (\e -> (l, e)) (\h -> (l, h)) eh) $ zip lins hs
            :: [Either (String, Text.Parsec.Error.ParseError) (String, H.HS)] in
  let hss = sort <$> T.sequenceA hs'
            :: Either (String, Text.Parsec.Error.ParseError) [(String, H.HS)] in
  let hss' = fmap (fmap (fmap (H.renamehs "end" "end'"))) hss in
  case hss' of
    Left (l, e) -> Left $ show e ++ " in " ++ l
    Right p -> Right $ L.intercalate "\n" $ map show $ process nonDec modul p

sort :: [(String, H.HS)] -> [(String, H.HS)]
sort xs =
  let sxs = H.sort (snd <$> xs) in
  let db = swap <$> xs in
  (\hs -> (M.fromMaybe "" $ L.lookup hs db, hs)) <$> sxs
  where swap (a, b) = (b, a)

process :: ED.NonDec -> String -> [(String, H.HS)] -> [Src]
process nonDec modul linsHask =
  let decls = M.mapMaybe (\(s, h) -> case h of { H.HSDecl d -> Just (s, d); _ -> Nothing}) linsHask in
  let (iodecls, otherdecls) = L.partition (isIODecl . snd) decls in
  let action_ctors = (\l -> zip l $ generate_action_ctors modul l) (map snd iodecls) in
  let rest = M.mapMaybe (\ x@(_, h) -> case h of { H.HSDecl _ -> Nothing; _ -> Just x}) linsHask in
  let restSrc = generate_rest nonDec rest in
  let (nondecActions, domainInfra) = generate_domain_infra nonDec modul action_ctors in
  L.intercalate [RawSrc ""] [
    [CSrc $ C.Comment "rests"],
    restSrc,
    [CSrc $ C.Comment "domain infra"],
    domainInfra,
    [CSrc $ C.Comment "iodecls"],
    generate_iodecls nondecActions iodecls $ map snd action_ctors,
    [CSrc $ C.Comment "otherdecls"],
    generate_decls otherdecls
  ]

generate_action_ctors :: String -> [H.Decl] -> [C.Ctor]
generate_action_ctors at = map (generate_action_ctor (at ++ "_action"))

generate_action_ctor :: String -> H.Decl -> C.Ctor
generate_action_ctor t d = case C2C.iOmap $ H2C.h2a (C.Term t) d of
                           C.Ctor n el -> C.Ctor n $ fmap (fmap (fmap C2C.coq2coqE)) el

data Src = CSrc C.Statement | RawSrc String | ResMatchStmt String [(C.Expr, C.Expr)]
instance Show Src where
  show (CSrc s) = C2S.coqshow s
  show (RawSrc s) = s
  show (ResMatchStmt name cases) =
    "Definition " ++ name ++ " action : Set :=\n" ++
    "  match action with" ++
    concat (map ("\n  | " ++) $ map (\ (caze, resT) -> C2S.coqshowE caze ++ " => " ++ C2S.coqshowE resT) cases) ++
    "\n  end."

generate_domain_infra :: ED.NonDec -> String -> [(H.Decl, C.Ctor)] -> (S.Set String, [Src])
generate_domain_infra nonDec modul action_ctors = 
    let ctorDecl = map (fmap (ED.getDec [] nonDec) . snd) :: [(a, Maybe C.Expr)] -> [Maybe (Either String [C.DecEqFun])] in
    let params c = case c of { C.Ctor _ p -> p } in
    let ctorIsDec = all (M.maybe False E.isRight) . ctorDecl . params in
    let (decC, nondecC) = L.partition (ctorIsDec . snd) action_ctors in
    let dndSep = if decC == [] || nondecC == [] then [] else [RawSrc ""] in
    let decS = if decC == [] then [] else ED.addDec nonDec [C.Inductive (modul ++ "_action") [] (map snd decC)] in
    let nonDecS = if nondecC == [] then [] else [C.Inductive (modul ++ "_nondec_action") [] (map snd nondecC)] in
    (S.fromList $ map ((\ (C.Ctor n _) -> n) . snd) nondecC,
    map CSrc decS ++
    dndSep ++
    map CSrc nonDecS ++
    [RawSrc ""] ++

    (if decC == [] then [] else generate_result_type (modul ++ "_action_res") decC) ++
    (if decC == [] then [] else generate_result_type_dec nonDec modul decC) ++
    dndSep ++
    (if nondecC == [] then [] else generate_result_type (modul ++ "_nondec_action_res") nondecC) ++
    [RawSrc ""] ++

    (if decC == [] then [] else generate_IO_alias_infra "" modul) ++
    dndSep ++
    (if nondecC == [] then [] else generate_IO_alias_infra "_nondec" modul)

    ++ (if decC == [] then [] else generate_IO_alias_infra__segment_props modul)
    )



generate_IO_alias_infra__segment_props :: String -> [Src]
generate_IO_alias_infra__segment_props modul =
     generate_IO_alias_infra__segment_unicity modul
  ++ generate_IO_alias_infra__segment_not_nil modul
  ++ generate_IO_alias_infra__segment_no_overlap modul



generate_IO_alias_infra__segment_no_overlap :: String -> [Src]
generate_IO_alias_infra__segment_no_overlap modul =
    [ CSrc $ C.Axiom (modul ++ "_segment_of_action_no_pre") $
      C.Forall
        [ ("a a'", Just $ C.Term $ modul ++ "_action")
        , ("res", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a"])
        , ("res'", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a'"])
        , ("s''' s'' s'", Just $ C.Term "(list msg')")
        , ("_", Just $ C.Arged
                  [ C.Term $ modul ++ "_segment_of_action_with_res"
                  , C.Term "a"
                  , C.Term "res"
                  , C.Term "(s'' ++ s')"
                  ])
        , ("_", Just $ C.Arged
                  [ C.Term $ modul ++ "_segment_of_action_with_res"
                  , C.Term "a'"
                  , C.Term "res'"
                  , C.Term "(s''' ++ s'')"
                  ])
        , ("_", Just $ C.Arged
                  [ C.Term "s'''"
                  , C.Term "<>"
                  , C.Term "nil"
                  ])
        , ("_", Just $ C.Arged
                  [ C.Term "s''"
                  , C.Term "<>"
                  , C.Term "nil"
                  ])
        , ("_", Just $ C.Arged
                  [ C.Term "s'"
                  , C.Term "<>"
                  , C.Term "nil"
                  ])
        ] (C.Term "False")
    ]



generate_IO_alias_infra__segment_not_nil :: String -> [Src]
generate_IO_alias_infra__segment_not_nil modul =
    [CSrc $ C.Axiom (modul ++ "_segment_of_action_not_nil") $
      C.Forall [
        ("a", Just $ C.Term $ modul ++ "_action"),
        ("res", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a"]),
        ("s", Just $ C.Term "(list msg')")
      ] $ C.Fun
            (C.Arged [C.Term $ modul ++ "_segment_of_action_with_res", C.Term "a", C.Term "res", C.Term "s"])
            (C.Arged [C.Term "s", C.Term "<>", C.Term "nil"])
    ]



generate_IO_alias_infra__segment_unicity :: String -> [Src]
generate_IO_alias_infra__segment_unicity modul =
    [CSrc $ C.Axiom (modul ++ "_segment_of_action_unicity") $
      C.Forall [
        ("a a'", Just $ C.Term $ modul ++ "_action"),
        ("res", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a"]),
        ("res'", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a'"]),
        ("s s'", Just $ C.Term "(list msg')")
      ] $ C.Fun
           (C.Arged [C.Term $ modul ++ "_segment_of_action_with_res", C.Term "a", C.Term "res", C.Term "s"])
            (C.Fun
              (C.Arged [C.Term $ modul ++ "_segment_of_action_with_res", C.Term "a'", C.Term "res'", C.Term "s'"])
              (C.Fun
                (C.Arged [C.Term "s", C.Term "=", C.Term "s'"])
                (C.Arged [C.Term "Beque.Util.inj_pair2b.depeq", C.Term $ "eq_" ++ modul ++ "_action_dec", C.Term "res", C.Term "res'"])))
    ]

generate_IO_alias_infra :: String -> String -> [Src]
generate_IO_alias_infra flag modul =
  [
    CSrc $ C.Axiom (modul ++ "_segment_of" ++ flag ++ "_action_with_res") $
      C.Forall [
        ("a", Just $ C.Term $ modul ++ flag ++ "_action"),
        ("res", Just $ C.Arged [C.Term $ modul ++ flag ++ "_action_res", C.Term "a"]),
        ("s", Just $ C.Term "(list msg')")
      ] (C.Term "Prop"),
    CSrc $ C.Definition ("IO" ++ flag) $
      C.Funall [ ("a", Just $ C.Term $ modul ++ flag ++ "_action") ] $
        C.Arged [
          C.Term "IO'",
          C.Funall [("_", Nothing)] (C.Term "True"),
          C.Arged [C.Term $ modul ++ flag ++ "_action_res", C.Term "a"],
          C.Funall [("r", Nothing), ("s", Nothing), ("_", Nothing)] $
            C.Arged [
              C.Term $ modul ++ "_segment_of" ++ flag ++ "_action_with_res",
              C.Term "a",
              C.Term "r",
              C.Term "s"
            ]
          ]
  ]

isIODecl :: H.Decl -> Bool
isIODecl (H.Decl _ _ t) = H2C.isIOtype t -- && H2C.isDecEq t

generate_result_type_dec :: ED.NonDec -> String -> [(H.Decl, C.Ctor)] -> [Src]
generate_result_type_dec nonDec modul ctors =
  let ress = sequence $ map (fmap (\ (_, _, x) -> x) . uncurry ctor2match) ctors :: Maybe [C.Expr] in
  let decS = case ress of
               Nothing -> error $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ " unexpected nothing in " ++ show ctors
               Just resTs -> (fmap (L.nub . concat) . sequence . map (ED.getDec [] nonDec)) resTs :: Either String [C.DecEqFun] in
  case decS of
    Left e -> [CSrc $ C.Comment $ "nondec: " ++ e]
    Right l ->
      let outro = "apply H_Beque_Util_result_eq_result_dec; try assumption; " ++ (if L.elem (C.DecEqFun "@Beque.Util.dec_utils.List_list_eq_dec") l then "try apply H_Beque_Util_dec_utils_List_list_eq_dec; " else "") in
      let ltac = ED.genLtac "destruct a; " outro l in
      [CSrc $ C.Definition ("eq_" ++ modul ++ "_action_res_dec") (C.Funall [("a", Just $ C.Term $ modul ++ "_action"), ("res1 res2", Just $ C.Arged [C.Term $ modul ++ "_action_res", C.Term "a"])] (C.Term $ ltac ++ " : {res1 = res2}+{res1<>res2}"))]

generate_result_type :: String -> [(H.Decl, C.Ctor)] -> [Src]
generate_result_type at ctors =
  [CSrc $ C.Comment $ at ++ " results"] ++
    let termOrArged cn l = case l of { [] -> C.Term cn; _ -> C.Arged $ map C.Term (cn:l) } in
    let match_postproc = map (\ (cn, args, resT) -> (termOrArged cn args, resT)) in
    let convo matches = case matches of {
      Nothing -> error $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ " unexpected nothing in " ++ show ctors;
      Just matches' -> if matches' == [] then [] else match_postproc matches' } in
    let matches = sequence $ map (uncurry ctor2match) ctors in
    [ResMatchStmt at $ convo matches]

ctor2match :: H.Decl -> C.Ctor -> Maybe (String, [String], C.Expr)
ctor2match (H.Decl _ tcl t) (C.Ctor an e) =
  let arm = an in
  let iob = case (C2C.coq2coqE . H2C.ht2ct . H.getBottom) <$> H2C.tc2t tcl t of
              Right (C.Arged [C.Term "IO", ioArg]) -> Just $ C.Arged [C.Term "result.result", ioArg, C.Term "String.string"]
              _ -> Nothing
  in
  let args = C.argNames 0 e in
  fmap (\iobe -> (arm, args, iobe)) iob


generate_coq :: ED.NonDec -> (String, H.HS) -> [C.Statement]
generate_coq nonDec (raw, h) =
          let statements = map C2C.coq2coq $ H2C.h2c (St.evalState (H.stmaphs iomap h) (0 :: Int)) in
          let statements' = ED.addDec nonDec statements in
          C.Comment (" " ++ raw ++ " ") :
          {-C.Comment (" " ++ show h ++ " ") :
            C.Comment (" " ++ show (H2C.h2c (St.evalState (H.stmaphs iomap h) 0)) ++ " ") :-}
          statements'

iomap :: (Num a, Eq a, Show a) => H.Type -> St.State a H.Type
iomap (H.Arged [H.Plain "IO", r]) =
          do
            x <- St.get
            St.put (x + 1)
            let pfx = if x == 0 then "" else show (x - 1)
            return $ H.Arged [H.Plain "IO'", H.Plain $ "pre" ++ pfx, r, H.Plain $ "post" ++ pfx]
iomap t = return t

generate_rest :: ED.NonDec -> [(String, H.HS)] -> [Src]
generate_rest nonDec l =
  let stmts = map (generate_coq nonDec) l
               ::[[C.Statement]] in
  let srcs = map (map (CSrc . C2C.coq2coq)) stmts
              ::[[Src]] in
  L.intercalate [RawSrc ""] srcs


generate_iodecls :: S.Set String -> [(String, H.Decl)] -> [C.Ctor] -> [Src]
generate_iodecls nondecActions decls action_ctors =
  let zz = zip decls action_ctors :: [((String, H.Decl), C.Ctor)] in
  let sl = map gen zz :: [[C.Statement]] in
  let cl = map (map CSrc) sl :: [[Src]] in
  let cl' = L.intercalate [RawSrc ""] cl :: [Src] in
  cl'
  where gen ((src, d), c) =
          [C.Comment src, gen_fun c, gen_extraction d c]
        gen_fun (C.Ctor casename e) =
          let fn = take (length casename - length "_action") casename in
          let args = C.argNames 0 e in
          let resT = C.Arged [C.Term $ if S.member casename nondecActions then "IO_nondec" else "IO", C.Arged $ map C.Term $ casename:args] in
          let fnT = if args == [] then resT else C.Forall (map (\a -> (a, Nothing)) args) resT in
          C.Axiom fn fnT
        gen_extraction (H.Decl _ _ _) (C.Ctor casename _) = 
          let fn = take (length casename - length "_action") casename in
          C.ExtractConstant fn fn

generate_decls :: [(String, H.Decl)] -> [Src]
generate_decls = (L.intercalate [RawSrc ""]) . (map (\p -> (CSrc $ C.Comment $ fst p):{-(CSrc $ C.Comment $ show $ snd p):-}((map (CSrc . C2C.coq2coq) . H2C.h2c . H.HSDecl . snd) p)))
