{-# LANGUAGE CPP #-}

module Coq2Str (coqshow, coqshowE) where
import Data.List

import Coq

data ShowForallOrFun = SFa | SFu

coqshow :: Statement -> String
coqshow (Comment s) = "(*" ++ s ++ "*)"
coqshow (Axiom n e) = "Axiom " ++ n ++ " : " ++ coqshowE e ++ "."
coqshow (Definition n e) = "Definition " ++ n ++ (if topTup e then " : Set" else "") ++ " := " ++ coqshowE2 e SFu ++ "."
  where topTup (Tuple _) = True
        topTup _ = False
coqshow (Inductive n pl c) =
  let head_ = "Inductive " ++ n ++ (foldr (\p ps -> ps ++ " " ++ p) "" $ map coqshowP pl) ++ " :=" in
  let ctors = concat $ map ("\n| " ++) $ map (coqshowC (inductiveType' n pl)) c in
  head_ ++ ctors ++ "."
coqshow (ExtractConstant n e) = "Extract Constant " ++ n ++ " => \"" ++ e ++ "\"."
coqshow (ExtractInductive n c m) = "Extract Inductive " ++ n ++ " => \"" ++ n ++ "\" [ \"" ++ intercalate "\" \"" c ++ "\" ]" ++ case m of { Just s -> " \"" ++ s ++ "\""; Nothing -> "" } ++ "."

forallshow :: (String, Maybe Expr) -> String
forallshow (n, Just e) = "(" ++ n ++ " : " ++ coqshowE' e 1000 ++ ")"
forallshow (n, Nothing) = n

coqshowE :: Expr -> String
coqshowE e = coqshowE2 e SFa

keywordmangle :: String -> String
keywordmangle "Type" = "Type'"
keywordmangle s = s

coqshowE2 :: Expr -> ShowForallOrFun -> String
coqshowE2 (Term s) _ = keywordmangle s
coqshowE2 (TypedTerm s t) _ = s ++ " : " ++ coqshowE' t 1000
coqshowE2 (Arged l) _ = intercalate " " $ map (\e -> coqshowE' e 1) l
coqshowE2 (Tuple l) _ = intercalate " * " $ map (\e -> coqshowE' e 3) l
coqshowE2 (Fun d cd) _ = coqshowE' d 5 ++ " -> " ++ coqshowE' cd 6
coqshowE2 (Forall p e) SFa = "forall " ++ (intercalate " " $ map forallshow p) ++ ", " ++ coqshowE' e 1000
coqshowE2 (Forall p e) SFu = "fun " ++ (intercalate " " $ map forallshow p) ++ " => " ++ coqshowE' e 1000
coqshowE2 (Funall p e) _ = "fun " ++ (intercalate " " $ map forallshow p) ++ " => " ++ coqshowE' e 1000
coqshowE2 (Apply (Apply (Term "forall") e) b) _ = "forall" ++ coqshowE' e 1000 ++ ", " ++ coqshowE' b 1000
coqshowE2 (Apply (Apply (Term "(fun a b => a -> b)") d) cd) _ = coqshowE' d 5 ++ " XX-> " ++ coqshowE' cd 6
coqshowE2 (Apply (Apply (Term "(fun a b => (a * b)%type)") t) u) _ = coqshowE' t 4 ++ " XX* " ++ coqshowE' u 3
coqshowE2 (Apply f v) _ = coqshowE' f 2 ++ " " ++ coqshowE' v 1

coqshowE' :: Expr -> Int -> String
coqshowE' (Term s) _ = keywordmangle s
coqshowE' e@(Arged _) cp | cp >= 2 = coqshowE e
coqshowE' e@(Tuple _) cp | cp >= 4 = coqshowE e
coqshowE' e@(Fun _ _) cp | cp >= 6 = coqshowE e
coqshowE' e@(Apply (Term "forall") _) cp | cp >= 6 = coqshowE e
coqshowE' e@(Apply (Apply (Term "(fun a b => a -> b)") _) _) cp | cp >= 6 = coqshowE e
coqshowE' e@(Apply (Apply (Term "(fun a b => (a * b)%type)") _) _) cp | cp >= 4 = coqshowE e
coqshowE' e _ = "(" ++ coqshowE e ++ ")"

coqshowP :: (String, Maybe Expr) -> String
coqshowP (n, Nothing) = n
coqshowP (n, Just e) = "(" ++ n ++ " : " ++ coqshowE e ++ ")"

coqshowC :: Expr -> Ctor -> String
coqshowC ty (Ctor cn nel) =
  let ces = coqshowE (buildCE nel) in
  keywordmangle cn ++ if ces == coqshowE ty then "" else (" : " ++ ces)
  where buildCE [] = ty
        buildCE ((Just n, e):t) = case buildCE t of { Forall l e' -> Forall ((n, e):l) e'; e' -> Forall [(n, e)] e' }
        buildCE ((Nothing, Nothing):t) = Fun (Term "_") $ buildCE t
        buildCE ((Nothing, Just e):t) = Fun e $ buildCE t
