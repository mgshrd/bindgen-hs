{-# LANGUAGE CPP #-}

module Main (main) where

import qualified Data.Char as C
import qualified Data.List
import qualified Data.List.Split
import qualified Data.Map as M
import qualified Data.Set as S

import qualified System.Exit
import qualified System.Environment as Env
import qualified System.IO as SIO

import qualified Bindgen

main :: IO ()
main = do
  argv <- Env.getArgs

  if argv == ["-v"] then putStrLn VERSION >> System.Exit.exitWith System.Exit.ExitSuccess else return ()

  let name = if argv == [] then "" else (head . Data.List.Split.splitOn "/") $ head argv

  input <- getContents
  let hasks = lines input

  nonDecL <- if length argv < 2 then return [] else (fmap lines $ readFile (argv !! 1))
  let nonDec = S.fromList $ "Set":map trim nonDecL

  specDecL <- if length argv < 3 then return [] else (fmap lines $ readFile (argv !! 2))
  let specDecKV'' = sequence $ map (splitAtt '=') specDecL
  specDecKV' <- case specDecKV'' of
                       Just kv -> return kv
                       Nothing -> putStrLn "bad dec file: missing '='" >>
                                  System.Exit.exitWith (System.Exit.ExitFailure 1)
  let specDecKV = map (\ (k, v) -> (trim k, trim v)) specDecKV'
  let specDec = M.fromList specDecKV

  case Bindgen.bindgen (nonDecx nonDec specDec) name hasks of
    Left e -> SIO.hPrint SIO.stderr e >> System.Exit.exitFailure
    Right s -> SIO.putStrLn s

trim :: String -> String
trim = reverse . dropWhile C.isSpace . reverse . dropWhile C.isSpace

splitAtt :: Eq a => a -> [a] -> Maybe ([a], [a])
splitAtt e l = fmap ((flip splitAt) l) $ Data.List.elemIndex e l

nonDecx :: S.Set String -> M.Map String String -> String -> Maybe (S.Set String)
nonDecx nds df =
  \s -> if S.member s nds
        then Nothing
        else if M.member s df
        then Just $ S.fromList [df M.! s]
        else Just $ S.fromList ([case s of
                          "Ascii.ascii" -> "Ascii.ascii_dec"
                          "String.string" -> "Strings.String.string_dec"
                          "bool" -> "Beque.Util.dec_utils.eq_bool_dec"
                          "list" -> "@Beque.Util.dec_utils.List_list_eq_dec"
                          "nat" -> "Beque.Util.dec_utils.PeanoNat_Nat_eq_dec"
                          "option" -> "@Beque.Util.dec_utils.eq_option_dec"
                          "result.result" -> "@Beque.Util.result.eq_result_dec"
                          "string" -> "string_dec"
                          "unit" -> "Beque.Util.dec_utils.eq_unit_dec"
                          s' -> "eq_" ++ s' ++ "_dec"])
