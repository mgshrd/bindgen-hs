module Hask (
  Typeclass(Typeclass),
  Type(Plain, Arged, Tuple, Fun),
  Decl(Decl), Ctor(Ctor), Data(Data), Type2(Type2), Kind(Kind),
  HS(HSDecl, HSData, HSType, HSKind),
  tmap, stmaphs, renamehs, getBottom, replaceBottom,
  sort,
) where

import Data.List (intercalate)
import Control.Monad.State

data Typeclass = Typeclass String [String] deriving (Eq, Ord, Show)
data Type =
    Plain String
  | Arged [Type]
  | Tuple [Type]
  | Fun Type Type
  deriving (Eq, Ord)
instance Show Type where
  show (Plain n) = "(P" ++ n ++ ")"
  show (Arged l) = "(A[" ++ (intercalate ", " (map show l)) ++ "])"
  show (Tuple t) = "(T" ++ intercalate ", " (map show t) ++ ")"
  show (Fun t1 t2) = "(F" ++ (show t1) ++ " -> " ++ (show t2) ++ ")"
data Decl = Decl String [Typeclass] Type deriving (Eq, Ord, Show)
data Ctor = Ctor String [(Maybe String {- record field extractor -}, Type)] deriving (Eq, Ord, Show)
data Data = Data String [String] [Ctor] deriving (Eq, Ord, Show)

data Type2 = Type2 String [String] Type deriving (Eq, Ord, Show)

data Kind = Kind String Integer deriving (Eq, Ord, Show)

data HS = HSDecl Decl | HSData Data | HSType Type2 | HSKind Kind deriving (Eq, Ord, Show)

sttmap :: (Type -> State s Type) -> Type -> State s Type
sttmap f x@(Plain _) = f x
sttmap f (Arged l) = do { l' <- mapM (sttmap f) l; f $ Arged l' }
sttmap f (Tuple l) = do { l' <- mapM (sttmap f) l; f $ Tuple l' }
sttmap f (Fun d cd) = do { d' <- sttmap f d; cd' <- sttmap f cd; f $ Fun d' cd' }

stmaphs :: (Type -> State s Type) -> HS -> State s HS
stmaphs f (HSDecl (Decl n tcl t)) = do { t' <- sttmap f t; return $ HSDecl (Decl n tcl t') }
stmaphs f (HSData (Data n p cl)) =
  fmap (HSData . Data n p) $ mapM stmapc cl
  where stmapc (Ctor cn fl) = fmap (Ctor cn) $ mapM (\(mn, t) -> sttmap f t >>= (\t' -> return (mn, t'))) fl
stmaphs f (HSType (Type2 n p t)) = do { t' <- sttmap f t; return $ HSType $ Type2 n p t' }
stmaphs _ k@(HSKind _) = return k

rename :: String -> String -> Type -> Type
rename from to tx = tmap (\t -> case t of { Plain a | from == a -> Plain to; ty -> ty }) tx

renamehs :: String -> String -> HS -> HS
renamehs from to (HSDecl (Decl n tcl t)) = HSDecl (Decl n tcl $ rename from to t)
renamehs from to (HSData (Data n p cl)) = HSData (Data n p $ map (\(Ctor cn fl) -> Ctor cn $ map (fmap (rename from to)) fl) cl)
renamehs from to (HSType (Type2 n p t)) = HSType (Type2 n p $ rename from to t)
renamehs _ _ k@(HSKind _) = k

tmap :: (Type -> Type) -> Type -> Type
tmap f t = evalState (sttmap (return . f) t) ()

getBottom :: Type -> Type
getBottom (Fun _ cd) = getBottom cd
getBottom t = t

replaceBottom :: Type -> Type -> Type
replaceBottom cd' (Fun d cd) = Fun d $ replaceBottom cd' cd
replaceBottom cd' _ = cd'

hsName :: HS -> String
hsName (HSDecl (Decl name _ _))  = name
hsName (HSData (Data name _ _))  = name
hsName (HSType (Type2 name _ _)) = name
hsName (HSKind (Kind name _))    = name

hsNames :: HS -> [String]
hsNames =  snd . (`runState` []) . stmaphs (\t -> case t of { Plain s -> do { modify (s:); return t; }; _ -> return t })

-- |
--
-- >>> sort [HSData (Data "A" [] [Ctor "A" []]), HSData (Data "B" [] [Ctor "B" []])]
-- [HSData (Data "A" [] [Ctor "A" []]),HSData (Data "B" [] [Ctor "B" []])]
--
-- >>> sort [HSData (Data "A" [] [Ctor "A" [(Nothing, Plain "B")]]), HSData (Data "B" [] [])]
-- [HSData (Data "B" [] []),HSData (Data "A" [] [Ctor "A" [(Nothing,(PB))]])]
--
-- >>> sort [HSData (Data "A" [] [Ctor "A" []]), HSData (Data "B" [] [Ctor "B" []]), HSData (Data "C" [] [Ctor "C" []]), HSData (Data "D" [] [Ctor "D" []])]
-- [HSData (Data "A" [] [Ctor "A" []]),HSData (Data "B" [] [Ctor "B" []]),HSData (Data "C" [] [Ctor "C" []]),HSData (Data "D" [] [Ctor "D" []])]
--
-- >>> sort [HSData (Data "A" [] [Ctor "A" []]), HSData (Data "B" [] [Ctor "B" [(Nothing, Plain "C")]]), HSData (Data "C" [] [Ctor "C" []]), HSData (Data "D" [] [Ctor "D" []])]
-- [HSData (Data "A" [] [Ctor "A" []]),HSData (Data "C" [] [Ctor "C" []]),HSData (Data "B" [] [Ctor "B" [(Nothing,(PC))]]),HSData (Data "D" [] [Ctor "D" []])]
--
-- >>> sort [HSData (Data "A" [] [Ctor "A" [(Nothing, Plain "C")]]), HSData (Data "B" [] [Ctor "B" []]), HSData (Data "C" [] [Ctor "C" [(Nothing, Plain "B")]])]
-- [HSData (Data "B" [] [Ctor "B" []]),HSData (Data "C" [] [Ctor "C" [(Nothing,(PB))]]),HSData (Data "A" [] [Ctor "A" [(Nothing,(PC))]])]
sort :: [HS] -> [HS]
sort xs =
  let sorted = sort' [] xs in
  if sorted == xs then sorted else sort sorted
  where

    sort' accu [] = accu
    sort' accu (a:t) = sort' (insert a accu) t

    insert a [] = [a]
    insert a (h:t) = if h `dependsOn` a then a:h:t else h:insert a t

-- |
--
-- >>> dependsOn (HSDecl (Decl "A" [] (Plain "B"))) (HSDecl (Decl "C" [] (Plain "D")))
-- False
--
-- >>> dependsOn (HSDecl (Decl "A" [] (Plain "B"))) (HSDecl (Decl "B" [] (Plain "D")))
-- True
--
-- >>> dependsOn (HSDecl (Decl "A" [] (Plain "B"))) (HSDecl (Decl "C" [] (Plain "B")))
-- False
--
-- >>> dependsOn (HSDecl (Decl "A" [] (Plain "B"))) (HSDecl (Decl "C" [] (Plain "A")))
-- False
dependsOn :: HS -> HS -> Bool
dependsOn obj dep = elem (hsName dep) (hsNames obj)
