DUMPHTML = links -width 500 -dump

RUNHASKELL ?= runhaskell

GHC ?= ghc
GHCFLAGS ?= -Wall

HLINT ?= hlint
HLINTFLAGS ?=

DOCTEST ?= doctest
DOCTESTFLAGS ?= 

GIT ?= git
MAJOR = 0
MINOR = 0
COMMITCOUNT = $(shell $(GIT) rev-list --count HEAD)
PATCH = $(shell expr $(COMMITCOUNT) - 6)
EXTRA ?= $(shell git symbolic-ref --short HEAD | sed -e 's/^master$$//' -e 's/^./-&/')
DIRTY ?= $(shell git status --short | tr -d \\n | sed -e 's/..*/_dirty-'`date +'%Y-%m-%d-%H_%M_%S'`'/')
VER = $(MAJOR).$(MINOR).$(PATCH)$(EXTRA)$(DIRTY)

SOURCES = main Bindgen Bindgen_eq_dec Str2Hask Hask Hask2Coq Coq Coq2Coq Coq2Str

bindgen$(EXTRA): $(SOURCES:%=src/%.hs)
	$(GHC) -DVERSION=\""$(VER)"\" -isrc $(GHCFLAGS) $< -o $@

test: $(SOURCES:%=src/%.hs)
	$(DOCTEST) -isrc $(DOCTESTFLAGS) $^

parse: $(addprefix src/, parse.hs Str2Hask.hs Hask.hs)
	cd $(dir $<) && $(GHC) $(GHCFLAGS) $(notdir $<) -o $@
	mv $(dir $<)/$@ $@

ver:
	echo $(VER)

lint:
	$(HLINT) $(HLINTFLAGS) src

clean:
	rm -f src/*.o src/*.hi bindgen

dist: src-dist bin-dist

bin-dist: bindgen
	tar czf bindgen-hs-bin-$(VER)-$(shell uname -s)-$(shell uname -m).tar.gz $^

src-dist: $(shell ls src/*.hs)
	tar -C src -czf bindgen-hs-src-$(VER).tar.gz $(notdir $^)
