<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml">
	<xsl:output method="text" encoding="UTF-8" indent="yes"/>

	<xsl:template match="html:head"/>
	<xsl:template match="html:body">
		<xsl:for-each select=".//html:div[@id='synopsis']/html:details/html:ul/html:li">
			<xsl:choose>
				<xsl:when test="contains(., '{')">
					<!-- record defs call -->
					<xsl:for-each select="text()|*">
						<xsl:apply-templates select="."/>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="."/>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:text>&#10;</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- record defs -->
	<xsl:template match="html:li[not(position() = last())]">
		<xsl:copy-of select="."/><xsl:text>; </xsl:text>
	</xsl:template>
	<xsl:template>
		<xsl:copy-of select="."/>
	</xsl:template>

</xsl:stylesheet>
